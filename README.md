Xeinax: Space Warrior
=====================================

## Screenshots ##

![main_menu.PNG](https://bitbucket.org/repo/xpeAgk/images/3190033812-main_menu.PNG)

![game.PNG](https://bitbucket.org/repo/xpeAgk/images/2224357083-game.PNG)

![menu_no_hover.PNG](https://bitbucket.org/repo/xpeAgk/images/3465177213-menu_no_hover.PNG)

![menu_hover.PNG](https://bitbucket.org/repo/xpeAgk/images/387675504-menu_hover.PNG)

![game_over.PNG](https://bitbucket.org/repo/xpeAgk/images/2021326332-game_over.PNG)